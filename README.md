### How run Jenkins on local machine?
```
docker-compose up
```

When Jenkins is up and running you can see it on **http://localhost:8080**.

At the first time you must insert the admin password that you can find on terminal, a.e.:
![image](https://drive.google.com/uc?id=1QeJyn5a39XQ7g_eXWpAFew2WgbdwAjee)

After that click on "Install suggested plugins".
After plugins installation is completed you can continue as admin click on bottom window "Create First Admin User".
Click on "Save and Finish" button and on "Start using Jenkins".

Jenkins homepage, a.e.:
![image](https://drive.google.com/uc?id=17kWuuNCp2hNdJRza83YFsptcDXum2KZL)

### How to create a simple job in Jenkins?
1. Click on "New Item"
2. enter an item name (a.e. test)
3. select "Pipeline"
4. click on "OK" button

Now you can configure the job:
1. click on or scroll down to "Pipeline" section
2. set Definition as "Pipeline script"
3. paste this pipeline in script section:
```
pipeline {
    agent any
    stages {
        stage('deploy') {
			steps {
				echo "Hello world!"
			}
        }
    }
}
```
4. click on "Apply" button
5. click on "Save" button

Click on "Build now" to run the pipeline, a.e.:
![image](https://drive.google.com/uc?id=1q8b30z3Pkdfq8THUy8U9VMWTe5jyWJjJ)

![image](https://drive.google.com/uc?id=1cGTKlG3XCziBRUZdkyRC7AHeKJ4GBcX4)

And you can see "Hello world!" in Jenkins console, a.e.:
![image](https://drive.google.com/uc?id=1WPMCkkpDafMLFOEc87pATapm0Jfh-s67)

## Jenkins and AWS

### Plugins list (install it on Jenkins):
* Amazon Web Services SDK
* CloudBees AWS Credentials
* Pipeline: AWS Steps

If you have AWS account follow these instructions:
##### AWS console (https://aws.amazon.com/console/)
1. Sign in on AWS console
2. "Services/Storage/S3" section
3. click on "Create bucket"
4. add a bucket name and select a region that you prefer
5. after that you move in "Services/Security, Identity, & Compliance/IAM"
6. select "Users" section
7. click on "Add user"
8. add an user name and select "Programmatic access"
9. select "Attach existing policies directly"
10. flag "AmazonS3FullAccess" and "Next: Tags"
11. add a key and if you want a value (it's optional)
12. click on "Next: review" button
13. click on "Create user" button
14. before click on "Close" I suggest to download CSV where are store the Access Key ID and Secret Access Key
15. click on "Close" button

##### Configure Jenkins
###### Add AWS credentials inside Jenkins
1. enter in "Credentials/System" and click on "Global credentials (unrestricted)"
![image](https://drive.google.com/uc?id=1opYm4CNGJ_fONml_TSvQ1SiIifbBObLZ)
2. Click on "Add Credentials"
3. Kind voice as "AWS Credentials", Scope voice as "Global"
4. insert the requests informations (ID, Access Key ID, Secret Access Key, and so on)

###### Define global variable in Jenkins
1. enter in "Manage Jenkins/Configure System/"
2. flags on "Environment variables"
3. add these global variable, a.e.:
```
Name: BUCKET_AWS_REGION
Value: eu-west-1

Name: BUCKET_AWS_NAME
Value: jenkins-test-bucket
```

###### Create a job that read Jenkinsfile from public Git repository
1. Click on "New Item"
2. insert a job name (a.e.: test)
3. select "Pipeline" and click on "OK" button
4. select "Pipeline" section
5. Definition voice as "Pipeline script from SCM"
6. SCM voice as "Git"
7. insert repository url and click "Apply" and "Save"
8. enter in job details and click on "Build now" to test the pipeline