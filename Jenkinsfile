import hudson.plugins.git.*
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder

String LATEST_GIT_TAG
String INPUT_BUILD_NUMBER

pipeline {
    agent any
	environment { 
		FILE_NAME = "player_external.tar"
	}
	options {
        skipDefaultCheckout true
    }
    stages {
		stage('Checkout') {
			steps {
				checkout scm
			}
    	}
		stage('Build') {
			steps {
				script {
					sh 'tar -cvf $FILE_NAME static/external.html static/social.html'
				}
			}
		}
		stage('Unit test') {
			steps {
				println("No unit test")
			}
		}
		stage('Bump version') {
            steps {
                script {
					withCredentials([[$class: 'UsernamePasswordMultiBinding', 
						credentialsId: '7d5b309e-08ad-4434-832d-49aec1869a79', 
						usernameVariable: 'GIT_USERNAME',
						passwordVariable: 'GIT_PASSWORD'
					]]) {
						retry(5) {
							try {
								setGitCredentials()
								LATEST_GIT_TAG = sh(script: 'git describe --abbrev=0 --tags', returnStdout: true)

								INPUT_BUILD_NUMBER = input message: 'Build version?', parameters: [string(defaultValue: "$LATEST_GIT_TAG", description: '', name: 'build_id')]
								sh "mv $FILE_NAME $FILE_NAME-${INPUT_BUILD_NUMBER}.dev"
							} catch (Exception e) {
								println("Error during bump version!")
							}
						}
					}
                }
            }   
        }
		stage('Create repository tag') {
			steps {			
				script {
					withCredentials([[$class: 'UsernamePasswordMultiBinding', 
						credentialsId: '7d5b309e-08ad-4434-832d-49aec1869a79', 
						usernameVariable: 'GIT_USERNAME',
						passwordVariable: 'GIT_PASSWORD'
					]]) {
						retry(5) {
							try {
								setGitCredentials()
								println 'Tagging version...'
								sh "git tag ${INPUT_BUILD_NUMBER} && git push origin --tags"
							} catch(Exception e) {
								println("Error during tagging version!")
							}
						}
					}
				}
			}
		}
		stage('Push artifact') {
			steps {
				// TODO: remove comment
				withAWS(region: "$BUCKET_AWS_REGION", credentials:'AWS_CREDENTIALS') {
					s3Upload(
						bucket: "$BUCKET_AWS_NAME", 
						workingDir:'', 
						includePathPattern:"${FILE_NAME}-${INPUT_BUILD_NUMBER}.dev", 
						path: "releases"
					)

					sh "rm ${FILE_NAME}-${INPUT_BUILD_NUMBER}.dev"
            	}
			}
		}
		stage('Download artifact') {
			steps {
				println('Download artifact')
				// TODO: remove comment
				withAWS(region: "$BUCKET_AWS_REGION", credentials:'AWS_CREDENTIALS') {
					s3Download(
						file: "${FILE_NAME}-${INPUT_BUILD_NUMBER}.dev", 
						bucket: "$BUCKET_AWS_NAME", 
						path: "releases/${FILE_NAME}-${INPUT_BUILD_NUMBER}.dev", 
						force: true
					)
				}
			}
		}
		stage('Uncompress artifact') {
			steps {
				sh "mv $FILE_NAME-${INPUT_BUILD_NUMBER}.dev $FILE_NAME"
				sh "mkdir uncompress && tar -xvf $FILE_NAME -C uncompress/"
			}
		}
		stage('Download environment variables') {
			steps {
				println 'Download environment variables'

				withAWS(region: "$BUCKET_AWS_REGION", credentials:'AWS_CREDENTIALS') {
					s3Download(
						file: "dev.json", 
						bucket: "$BUCKET_AWS_NAME", 
						path: "dev.json", 
						force: true
					)
				}

				script {
					try {
						def props = readJSON file: 'dev.json'
						println(props['BASE_URL'])
						println(props.BASE_URL)
					} catch(Exception e) {
						println("Error during read JSON configuration")
					}
				}
			}
		}
		stage('Delete uncompresses files') {
			steps {
				script {
					sh 'rm -rf player_external.tar uncompress/'
				}
			}
		}
    }
}

String getGitBranchName() {
    return scm.branches[0].name - "*/"
}

void setGitCredentials() {
    sh("git config --global user.name ${env.GIT_USERNAME}")
    sh("git config credential.username ${env.GIT_USERNAME}")
}